const Course = require('../models/Course')
const auth = require('../auth')


module.exports.addCourse = (body) => {
	let newCourse = new Course({
		name : body.name,
		description : body.description,
		price : body.price
	})

	return newCourse.save().then((course, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
} 



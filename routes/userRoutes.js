const express = require('express')
const router = express.Router()
const userController = require('../controllers/userControllers')
const auth = require('../auth')


router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(result => {
		res.send(result)
	})
})


router.post('/register', (req, res)=> {
	userController.registerUser(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

router.get('/details' , auth.verify,  (req, res)=> {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({userId : userData.id}).then(result => {
		if(!result) {
			return res.status(500).send("User not found")
		} else {
			return res.send(result)
		}
	})
})





module.exports = router
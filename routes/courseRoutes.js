const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseControllers')
const auth = require('../auth')

router.post('/', auth.verify, auth.verifyAdmin,  (req, res) => {
	courseController.addCourse(req.body).then(result => res.send(result))
})





module.exports = router
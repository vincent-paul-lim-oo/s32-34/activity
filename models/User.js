const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "fName is required."]
	}, 
	lastName : {
		type :String,
		required : [true, "lName is required."]
	},
	email : {
		type :String,
		required : [true, "email is required."]
	},
	password : {
		type :String,
		required : [true, "password is required."]
	},
	isAdmin : {
		type :Boolean,
		default : false
	},
	mobileNo : {
		type :String,
		required : [true, "mobile is required."]
	},
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type: String,
				default : "Enrolled"
			}
		}
	]

}) 


module.exports = mongoose.model("User", userSchema)